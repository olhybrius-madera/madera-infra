# Madera Infra

Ce projet regroupe les composants d'infrastructure du projet Madera :
- Une instance de la plate-forme d'administration de bases de données PostgreSQL [PgAdmin](https://www.pgadmin.org) (optionnelle)
- Une instance du broker de message [RabbitMQ](https://www.rabbitmq.com/)
- Une instance du service de gestion des identités et d'authentification [Keycloak](https://www.keycloak.org/)

## Prérequis
Le seul logiciel nécessaire à l'utilisation du projet est [Docker](https://www.docker.com/).

## Variables d'environnement
Le projet s'appuie essentiellement sur des variables d'environnement à définir dans un fichier .env à la racine du projet. Les valeurs à renseigner sont les suivantes :

- **USER_ID** : L'ID de l'utilisateur au sein du container. Sous Linux, doit être égal à l'ID de votre utilisateur courant. Sous Windows, peu importe.
- **RABBITMQ_NAME** : Le nom et nom d'hôte du service RabbitMQ
- **RABBITMQ_PORT** : Le port d'écoute du service RabbitMQ
- **RABBITMQ_USER** : L'utilisateur du service RabbitMQ
- **RABBITMQ_PASSWORD** : Le mot de passe de l'utilisateur du service RabbitMQ
- **RABBITMQ_MANAGEMENT_PORT** : Le port d'écoute du service de gestion de l'instance RabbitMQ
- **KEYCLOAK_POSTGRES_DB** : Le nom de la base de données de Keycloak
- **KEYCLOAK_POSTGRES_USER** : L'utilisateur de la base de données de Keycloak
- **KEYCLOAK_POSTGRES_PASSWORD** : Le mot de passe de la base de données de Keycloak 
- **KEYCLOAK_USER** : L'utilisateur du service Keycloak
- **KEYCLOAK_PASSWORD** : Le mot de passe de l'utilisateur du service Keycloak
- **KEYCLOAK_PORT** : Le port d'écoute du service Keycloak

Les variables d'environnement suivantes sont optionnelles et ne sont requises que si l'on souhaite [disposer d'une instance de PgAdmin avec le reste de l'infrastructure](#pgadmin) :

- **PGADMIN_USER** : L'utilisateur du service PgAdmin
- **PGADMIN_PASSWORD** : Le mot de passe de l'utilisateur du service PgAdmin
- **PGADMIN_PORT** : Le port d'écoute du service PgAdmin

## Démarrer le projet
Lancer l'une des commandes suivantes à la racine du projet :

- <a name=pgadmin></a>Pour avoir une instance de PgAdmin :
  ```
  docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
  ```

- Pour lancer le projet sans PgAdmin :
  ```
  docker-compose up -d
  ```

## Arrêter le projet
Lancer l'une des commandes suivantes à la racine du projet :

- Si l'on a lancé l'infrastructure complète (avec PgAdmin) : 
  ```
  docker-compose -f docker-compose.yml -f docker-compose.dev.yml down -d
  ```

- Sinon :
  ```
  docker-compose down -d
  ```


